#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\rt.h"
#include "passengers.h"

#include <stdio.h>
#include <iostream>

#define up 1
#define down 0
#define closed 1
#define open 0
#define END 1000
#define startFault 2000
#define endFault 2001

int main()
{
	CPipe passPipe("passengers", 1024);
	int waitTime;
	int passCounter = 0;
	int desiredFloor;
	int direction;
	int initFloor;
	passenger *passengers[30];

	while (1)
	{
		desiredFloor = rand() % 10;
		initFloor = rand() % 10;
		while (initFloor == desiredFloor)
			initFloor = rand() % 10;
		if (desiredFloor > initFloor)
			direction = up;
		else
			direction = down;
		passengers[passCounter] = new passenger(passCounter, direction, initFloor, desiredFloor);
		waitTime = rand() % 1000;

		passPipe.Write(&passengers[passCounter], sizeof(passenger);

		Sleep(1000);
		if (passCounter >= 29)
			passCounter = 0;
		else
			passCounter++;
	}
}